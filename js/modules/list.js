$(function() {
    const { categories, products, files } = JSON.parse(localStorage.getItem('deviceDetails'))

    // console.log('test data', JSON.parse(localStorage.getItem('deviceDetails')))

    const categId = getUrlParam('id')

    const category = categories.filter(x => x.id === categId)[0]
    // console.log('category', category)

    $('#listHeadline').html(category.name)

    const categProducts = products.filter(x => category.products.includes(x.id))
    // console.log('categProducts', categProducts)

    // append html to productList id div
    let productContainer = ''
    categProducts.map(x => {
        productContainer += listComponent(x, files)
    })
    $('#productList').html(productContainer)

    // hide if less than 4 items
    if ($('.list-list-item').length < 5)  { 
        $('.list-next').hide(); $('.list-prev').hide()
    }

    //ellipsis alternative
    function loadeEllipsis() {
        //Description solution
        $('.list-item-description').each(function() {
            let wordArray = this.innerHTML.split(' ');
            wordArray.pop();
            while(this.scrollHeight > this.offsetHeight) {
                wordArray.pop();
                this.innerHTML = wordArray.join(' ') + '...';
            }
        });

        //Model solution
        $('.list-item-model').each(function() {
            while(this.scrollHeight > this.offsetHeight) {
                let style = window.getComputedStyle(this, null).getPropertyValue('font-size');
                let fontSize = parseFloat(style);
                this.style.fontSize = (fontSize - 1) + 'px';
            }
        });
    }
    loadeEllipsis();
    setTimeout(loadeEllipsis, 500);
})

function listComponent(data, files) {
    const promo = data.attributes.filter(x => x.attribute === '5f7569bae96e20160f6b6d4f')[0]
    const shortDescription = data.attributes.filter(x => x.attribute === '5f756aaee96e20160f6b6d55')[0]

    const defaultImg = data.images.filter(x => x.default)

    const image = data.images.length <= 0 
        // ? 'https://via.placeholder.com/1200' 
        ? 'imgs/default_img.png'
        : host + files.filter(file => file.id == defaultImg[0].file)[0].path

    const price = formatCurrency(data.price)
    
    return `
        <a href="product.html?code=${data.code}">
            <div class="list-list-item">
                <img class="list-item-image" src="${image}">
                <div class="list-item-model">${data.name}</div>
                
                <!-- <div class="list-item-brand">PROFORM</div> -->
                
                <div class="list-item-price-background"></div>
                <div class="list-item-price">
                    ${
                        (promo.value) 
                            ? `<span class="strike">${price}</span>` 
                            : `<span>${price}</span>` 
                    }
                    <span class="list-item-price-promo">
                        ${
                            (promo.value)
                                ? `${formatCurrency(promo.value)}<div class="promo-figure"><span class="promo-text">PROMO</span></div>`
                                : ''
                        }
                    </span>
                </div>
                <div class="list-item-description">${shortDescription.value || ''}</div>
            </div>
        </a>
    `
}