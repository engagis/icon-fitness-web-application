$(function() {
    const { categories } = JSON.parse(localStorage.getItem('deviceDetails'))
    // console.log('categories', categories)
    // categories data
    const treadmill = categories[1]
    const elliptical = categories[2]
    const exerciseBikes = categories[3]
    const rowers = categories[4]
    const accessories = categories[6]
    const strength = categories[5]

    // update href url added category id
    $('#treadmill-link').attr('href', `list.html?id=${treadmill.id}`)
    $('#elliptical-link').attr('href', `list.html?id=${elliptical.id}`)
    $('#exerciseBikes-link').attr('href', `list.html?id=${exerciseBikes.id}`)
    $('#rowers-link').attr('href', `list.html?id=${rowers.id}`)
    $('#accessories-link').attr('href', `list.html?id=${accessories.id}`)
    $('#strength-link').attr('href', `list.html?id=${strength.id}`)

    // update the src of the image per category
    // $('#treadmill img').attr('src', `${host}${getFileById(treadmill.image).path}`)
    // $('#elliptical img').attr('src', `${host}${getFileById(elliptical.image).path}`)
    // $('#exerciseBikes img').attr('src', `${host}${getFileById(exerciseBikes.image).path}`)
    // $('#rowers img').attr('src', `${host}${getFileById(rowers.image).path}`)
    // $('#accessories img').attr('src', `${host}${getFileById(accessories.image).path}`)
    // $('#strength img').attr('src', `${host}${getFileById(strength.image).path}`)

})
