$(function() {
    const { categories, products, files } = JSON.parse(localStorage.getItem('deviceDetails'))

    const code = getUrlParam('code')

    const product = products.filter(x => x.code === code)[0]

    const category = categories.filter(x => x.products.includes(product.id))[0]
    const categProducts = products.filter(x => category.products.includes(x.id))
    // console.log('products', products)

    $('#breadcrumbLink').attr('href', `list.html?id=${category.id}`)
    $('#breadcrumbText').html(category.name)

    $('#productContent').html(productContentComponent(product))
    $('#prodImg').html(productImgComponent(product, files))


    /**
     * limmuel configuration
     */
    $('.prod-slideWrap').slick({
        'autoplay': true,
        'autoplaySpeed':'3000',
    })

    $('.prod-btnPrev').on('click', function(e){
        e.preventDefault();
        $(this).parent().find('.prod-slideWrap').slick('slickPrev');
    });
    $('.prod-btnNext').on('click', function(e){
        e.preventDefault();
        $(this).parent().find('.prod-slideWrap').slick('slickNext');
    });
    
    var slideCount = $('.prod-slideWrap').find('.prod-slide:not(.slick-cloned)').length
    if(slideCount < 2){
        $('.prod-btnPrev,.prod-btnNext').hide();
    }else{
        $('.prod-btnPrev,.prod-btnNext').show();
    }

    // price
    var hasPromo = $('.prod-priceHasPromo').text()
    if(hasPromo.trim() != ''){
        $('.prod-priceWrap').has('.prod-priceHasPromo').find('.prod-price').css({
        'opacity':'.6',
        'text-decoration':'line-through'
        })
    }else{    
        $('.prod-promoLabel').hide()
    }
    /**
     * limmuel configuration end
     */

    // arrow left and right to change page
    // $('.prod-btnPrev').on('click', function(e){
    //     e.preventDefault()
    //     for (let i = 0; i < categProducts.length; i++) {
    //         if(categProducts[i].code === code) {
    //             const code = ((i - 1) < 0) 
    //                 ? categProducts[categProducts.length-1].code
    //                 : categProducts[i-1].code

    //             window.location.href = `product.html?code=${code}`
    //             break
    //         }
    //     }
    // })
    // $('.prod-btnNext').on('click', function(e){
    //     e.preventDefault()
    //     for (let i = 0; i < categProducts.length; i++) {
    //         if(categProducts[i].code === code) {
    //             const code = ((i + 1) === categProducts.length)
    //                 ? categProducts[0].code
    //                 : categProducts[i+1].code

    //             window.location.href = `product.html?code=${code}`
    //             break
    //         }
    //     }
    // })

})

function productContentComponent(data) {
    const promo = data.attributes.filter(x => x.attribute === '5f7569bae96e20160f6b6d4f')[0]
    const specifications = data.attributes.filter(x => x.attribute === '5f7e9897e96e20160f6ccffd')[0]
    const features = data.attributes.filter(x => x.attribute === '5f756a66e96e20160f6b6d53')[0]
    
    return `
        <div class="prod-name">${data.name}</div>
        <div class="prod-modelName">${data.code}</div>
        <div class="prod-priceWrap">
        <div class="prod-price">${ formatCurrency(data.price) }</div>
        <div class="prod-priceHasPromo">
            ${ promo.value ? `${ formatCurrency(promo.value) }` : '' }
        </div>
        </div>
        <div class="prod-promoLabel">promo</div>
        <div class="prod-context">
            <div class="prod-context-desc">
            <h3>specifications:</h3>
            ${ specifications.value ? specifications.value : '' }
            </div>
            <div class="prod-context-feat">
            <h3>features:</h3>
            ${ features.value ? features.value : '' }
            </div>
        </div>
    `
}


function productImgComponent({ images }, files) {

    // const image = images.length <= 0 
    //     // ? 'imgs/products/sampleProduct.jpg' 
    //     ? ''
    //     : host + files.filter(file => file.id == images[0].file)[0].path

    let container = ''

    images.map(img => {
        const imgSrc = host + files.filter(file => file.id == img.file)[0].path
        container += `
            <div class="prod-slide">
                <img src="${imgSrc}" alt="">
            </div>
        `
    })
    // console.log('container', container)
    return container
    // return `
    //     <div class="prod-slide">
    //         <img src="${image}" alt="">
    //     </div>
    // `
}