const host = "http://localhost:3000";
// Declare variables
var idleTime = 0;

// Load when document is ready
$(function(){
	
	// ajax request get data from localhost:3000 server
	$.ajax({
		async: false,
        url: `${host}/local?appCode=ezefind`,
        type: 'GET',
        dataType: 'json',
        success: function(res) {
			localStorage.removeItem('deviceDetails');
			localStorage.setItem('deviceDetails', JSON.stringify(res));
        }
	})
	
	// load data from json and save to localstorage
	// $.ajax({
	// 	async: false,
	// 	url: 'js/data/local.json',
	// 	dataType: 'json',
	// 	success: function (res) {
	// 		localStorage.removeItem('deviceDetails');
	// 		localStorage.setItem('deviceDetails', JSON.stringify(res));
	// 	}
	// });

	// Load nav on id placeholder
	$("#nav-placeholder").load("nav.html");

	// Adding splash screen
	$('body').prepend("<div class='splash-screen'></div>")
	$(".splash-screen").click(function(){
		$(".splash-screen").fadeOut();
		window.location.replace("index.html");
	});


    //Increment the idle time counter every second.
    var idleInterval = setInterval(timerIncrement, 1000);

    //Zero the idle timer on action.
    $(this).click(function (e) {
    	idleTime = 0;
    });
    $(this).scroll(function (e) {
    	idleTime = 0;
    });
    $(this).keypress(function (e) {
    	idleTime = 0;
    });

});

function timerIncrement() {
	idleTime = idleTime + 1;
	if (idleTime > 10000) {
		$(".splash-screen").fadeIn();
	}
}

// get products from localstorage data
function getProductionsByCateg(serviceId, array = []) {
	const data = array.length <= 0
		? JSON.parse(localStorage.getItem('deviceDetails'))
		: array

	// service data
    const service = data.categories.filter( x => x.id === serviceId)[0]
    //service product data
    const products = data.products.filter( x => service.id === x.category)

    return products
}

// get url param by type
function getUrlParam(type) {
    const url = new URL(location.href)
    return url.searchParams.get(type)
}

// get the file data by id
function getFileById(fileId, array = []) {
	const data = array.length <= 0
		? JSON.parse(localStorage.getItem('deviceDetails'))
		: array

	const file = data.files.filter( x => x.id === fileId)[0]

	return file

}

// get formatted currency
function formatCurrency(value) {
	// Create our number formatter.
	let formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD',
	
		// These options are needed to round to whole numbers if that's what you want.
		minimumFractionDigits: 2,
		maximumFractionDigits: 20,
	})
	
	return formatter.format(value) 
}
